﻿namespace DailyProgrammer.Hard

open System
open System.IO

module H1 = 
    let rec guess low high = 
        let mid = (low + high)/2
        printfn "Is %d equal, too high, or too low?" mid
        let inp = Console.ReadLine ()
        match inp with
        | "equal" -> printfn "I knew it!"
        | "low" -> guess (mid+1) high
        | "high" -> guess low (mid-1)
        | _ -> ()

    let main = 
        printfn "Pick a number between 1 and 100..."
        guess 1 100

module H2 = 
    let start () =
        let mutable t1 = DateTime.Now
        let mutable last = t1
        let mutable inp = Console.ReadLine ()
        let mutable running = true
        while running do
            match inp with
            | "stop" -> 
                let t2 = DateTime.Now
                let time = (t2 - t1).TotalSeconds
                printfn "Time: %f" time
                printfn "resume, reset, or quit?"
                let inp2 = Console.ReadLine ()
                match inp2 with
                | "reset" ->
                    t1 <- DateTime.Now
                    last <- t1
                    inp <- Console.ReadLine ()
                | "resume" -> // kinda broken
                    inp <- Console.ReadLine ()
                | _ -> running <- false
            | "lap" ->
                let t2 = DateTime.Now
                let time = (t2 - last).TotalSeconds
                last <- t2
                printfn "Lap: %f" time
                inp <- Console.ReadLine ()
            | _ -> inp <- Console.ReadLine ()

    let main () =
        printfn "Stopwatch start"
        start ()

module H3 =
    let find (scramble : string) (wordlist : string []) =
        Array.find (fun (word : string) ->
            String.Concat (Array.sort (word.ToCharArray () )) = String.Concat (Array.sort (scramble.ToCharArray () ))
        ) wordlist

    let main () =
        let words = File.ReadAllLines "Hard/wordlist.txt"
        let scrambled = File.ReadAllLines "Hard/scrambled.txt"
        let keys = Array.map (fun scr -> find scr words) scrambled
        for k in keys do
            Console.WriteLine k
        ()

module H4 =
    let permute (arr : int []) = // hardcoded hack
        let p = seq {
            yield [| arr.[0] ; arr.[1] ; arr.[2] |]
            yield [| arr.[0] ; arr.[2] ; arr.[1] |]
            yield [| arr.[1] ; arr.[0] ; arr.[2] |]
            yield [| arr.[1] ; arr.[2] ; arr.[0] |]
            yield [| arr.[2] ; arr.[1] ; arr.[0] |]
            yield [| arr.[2] ; arr.[0] ; arr.[1] |]
        }
        Set.ofSeq p

    let main () =
        Console.Write "Input: "
        let inp = (Console.ReadLine ()).Split () |> Array.map int
        inp |> permute |> Set.iter (fun arr ->
            if arr.[0] + arr.[1] = arr.[2] then
                Console.WriteLine (string arr.[0] + " + " + string arr.[1] + " = " + string arr.[2])
            elif arr.[0] - arr.[1] = arr.[2] then
                Console.WriteLine (string arr.[0] + " - " + string arr.[1] + " = " + string arr.[2])
            elif arr.[0] * arr.[1] = arr.[2] then
                Console.WriteLine (string arr.[0] + " * " + string arr.[1] + " = " + string arr.[2])
            elif arr.[0] / arr.[1] = arr.[2] then
                Console.WriteLine (string arr.[0] + " / " + string arr.[1] + " = " + string arr.[2])
        )
        ()

module H5 =
    // technically state should not be mutable, instead generating a new state with changed values
    // but then real life gets in the way...
    type GameState = {
        mutable p1hp : int
        mutable p2hp : int
        mutable gameover : bool
        rand : Random
    }

    let hp state pnum =
        match pnum with
        | 1 -> state.p1hp
        | 2 -> state.p2hp

    let fight state pnum dmg =
        match pnum with
        | 1 -> state.p2hp <- state.p2hp - dmg
        | 2 -> state.p1hp <- state.p1hp - dmg
        if state.p1hp < 0 || state.p2hp < 0 then
            Console.WriteLine "The scoundrel has gone to Davey Jones' locker!"
            state.gameover <- true
        state

    let heal state pnum amt =
        match pnum with
        | 1 -> state.p1hp <- state.p1hp + amt
        | 2 -> state.p2hp <- state.p2hp + amt
        state

    let playeraction num state =
        Console.WriteLine ()
        Console.WriteLine ("Pirate " + string num + ", ya got " + string (hp state num) + " HP. What be yer action?")
        let act = Console.ReadLine ()
        match act with
        | "sword" ->
            Console.WriteLine "Ya take a swing at the landlubber wit yer cutlass!"
            let dmg = state.rand.Next 10
            Console.WriteLine ("He bleeds fer " + string dmg + " points o' damage.")
            fight state num dmg
        | "pistol" ->
            Console.WriteLine "Ya raise yer flintlock and fire!"
            if state.rand.NextDouble () < 0.5 then
                Console.WriteLine "Ya miss, ya scurvy rat."
                state
            else
                let dmg = state.rand.Next 20
                Console.WriteLine ("Ya deal " + string dmg + " points o' damage to the salty swab.")
                fight state num dmg
        | "orange" ->
            Console.WriteLine "Ya grab an orange and take a hearty bite."
            let amt = state.rand.Next 15
            Console.WriteLine ("Ya heal yer scurvy for " + string amt + " points.")
            heal state num amt
        | "help" ->
            Console.WriteLine "Ya spend a turn readin' yer manual."
            Console.WriteLine "Valid moves are 'sword', 'pistol', and 'orange'."
            state
        | _ ->
            Console.WriteLine "Ya fumble around in confusion."
            state

    let rec play state =
        if state.gameover then
            ()
        else
            state |> playeraction 1 |> playeraction 2 |> play

    let main () =
        play { p1hp = 100; p2hp = 100; gameover = false; rand = new Random () }
        ()