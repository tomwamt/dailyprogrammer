﻿namespace DailyProgrammer.Hard

open System

module H11 =
    open System.Globalization

    let isToday (dt : DateTime) =
        dt.Date = DateTime.Now.Date

    let rec printDay month (dt : DateTime) =
        if dt.Month <> month then
            ()
        elif dt.DayOfWeek = DayOfWeek.Saturday then
            Console.Write " "
            if isToday dt then
                Console.BackgroundColor <- ConsoleColor.White
                Console.ForegroundColor <- ConsoleColor.Black
            Console.WriteLine (sprintf "%2d" dt.Day)
            Console.ResetColor ()
            printDay month (dt.AddDays 1.0)
        else
            Console.Write " "
            if isToday dt then
                Console.BackgroundColor <- ConsoleColor.White
                Console.ForegroundColor <- ConsoleColor.Black
            Console.Write (sprintf "%2d" dt.Day)
            Console.ResetColor ()
            printDay month (dt.AddDays 1.0)

    let main () =
        Console.Write "Month: "
        let month = int (Console.ReadLine ())
        Console.Write "Year: "
        let year = int (Console.ReadLine ())
        let dt = DateTime (year, month, 1)
        let dow = dt.DayOfWeek
        Console.WriteLine ((DateTimeFormatInfo.CurrentInfo.GetMonthName month) + " " + (string year))
        Console.WriteLine " Su Mo Tu We Th Fr Sa"
        Console.Write (String.replicate (3 * (int dow)) " ")
        printDay month dt
        Console.WriteLine ()
        ()

module H12 =
    open System.IO
    open System.Media

    let tones = dict [
                    ("A3", 220.00)
                    ("B3", 246.94)
                    ("C4", 261.63)
                    ("D4", 293.66)
                    ("E4", 329.63)
                    ("F4", 349.23)
                    ("Fsharp4", 369.99) // Hehe F#
                    ("G4", 392.00)
                ]

    type Note = { freq : float; dur : float }

    let playNotes (notes : Note []) (player : SoundPlayer) =
        let sampleRate = 44100
        let channels = 1s
        let bitsPerSample = 16s
        let byteRate = sampleRate * int channels * int (bitsPerSample/8s)
        let totalDur = Array.sumBy (fun n -> n.dur) notes
        let dataSize = int (float byteRate * totalDur) // in bytes
        let blockAlign = channels * (bitsPerSample/8s)

        use stream = new MemoryStream ()
        use writer = new BinaryWriter (stream) // BinaryWriter is little-endian, so big-endian fields are reversed
        // header
        writer.Write 0x46464952 // "RIFF"
        writer.Write (36+dataSize) // size of file - 8
        writer.Write 0x45564157 // "WAVE"
        // fmt
        writer.Write 0x20746d66 // "fmt "
        writer.Write 16 // size of chunk
        writer.Write 1s // PCM
        writer.Write channels
        writer.Write sampleRate
        writer.Write byteRate
        writer.Write blockAlign
        writer.Write bitsPerSample
        // data
        writer.Write 0x61746164 // "data"
        writer.Write dataSize

        let ampl = 10000.0

        notes
        |> Array.iter (fun note ->
            let numSamples = int (note.dur * float sampleRate)

            seq {1..numSamples}
            |> Seq.map (fun i -> 
                let t = (float i) / (float sampleRate)
                int16 (ampl * Math.Sin (t * note.freq * 2.0 * Math.PI))
            )
            |> Seq.iter writer.Write
        )

        writer.Flush ()
        stream.Seek (0L, SeekOrigin.Begin) |> ignore

        player.Stream <- stream
        player.PlaySync ()

    let strsplit sep (str : string) =
        str.Split [| sep |]

    let main args =
        let input = Console.ReadLine ()
        let notes = input |> strsplit ' ' |> Array.map (fun s -> {freq = tones.[s] ; dur = 0.5})
        use player = new SoundPlayer ()
        playNotes notes player
        ()

module H13 =
    let choices = [| "rock" ; "paper" ; "scissors" |]
    let rand = Random ()

    let rpsplay () =
        // This can be mathematical proven to be the optimal solution
        let ai1choice = rand.Next 3
        let ai2choice = rand.Next 3
        if ai1choice = ai2choice then
            Console.WriteLine ("Draw (" + choices.[ai1choice] + ")")
        elif ai1choice = (ai2choice + 1) % 3 then
            Console.WriteLine ("AI 1 wins (" + choices.[ai1choice] + " > " + choices.[ai2choice] + ")")
        else
            Console.WriteLine ("AI 2 wins (" + choices.[ai2choice] + " > " + choices.[ai1choice] + ")")

    let main args =
        Seq.iter (fun i -> rpsplay ()) (seq {1..10})
        ()

module H14 =
    let sortwf arr = async { return Array.sort arr }

    let merge arrs =
        let rec loop lists acc =
            if Array.forall List.isEmpty lists then
                acc |> Array.ofList |> Array.rev
            else
                let mini = lists |> Array.indexed |> Array.minBy (fun (i, ls) -> if ls.IsEmpty then Double.MaxValue else ls.Head) |> fst
                let minv = lists.[mini].Head
                let newLists = lists |> Array.indexed |> Array.map (fun (i, ls) -> if i = mini then ls.Tail else ls)
                loop newLists (minv :: acc)

        loop (Array.map List.ofArray arrs) []


    let psort arr p =
        arr
        |> Array.splitInto p
        |> Array.map (fun chunk -> sortwf chunk)
        |> Async.Parallel
        |> Async.RunSynchronously
        |> merge

    let main (args : string []) =
        let rand = Random ()
        let arr = [| for i in 1..16000000 -> 100.0 * rand.NextDouble () |]
        let p = int (args.[0])
        let start = DateTime.Now
        psort arr p |> ignore // It's so slow... :(
        let stop = DateTime.Now
        Console.WriteLine ("Done with my sort in " + string (stop - start).TotalSeconds)
        let start = DateTime.Now
        Array.sort arr |> ignore
        let stop = DateTime.Now
        Console.WriteLine ("Done with builtin sort in " + string (stop - start).TotalSeconds)
        ()

module H15 =
    open System.Net
    open System.Net.Sockets
    open System.Text
    open System.Threading.Tasks

    let handleConnection (handler : Socket) () =
        Console.WriteLine ("Connection from " + string handler.RemoteEndPoint)

        let buffer = Array.init 1024 (fun _ -> 0uy)
        let recv = handler.Receive buffer
        let data = Encoding.UTF8.GetString (buffer, 0, recv)

        Console.WriteLine ("Received message: " + data)

        let resp = data.ToCharArray () |> Array.rev |> String
        resp |> Encoding.UTF8.GetBytes |> handler.Send |> ignore

        Console.WriteLine ("Response sent: " + resp)

        handler.Shutdown SocketShutdown.Both
        handler.Close ()
        ()

    let server port =
        let ipHostInfo = () |> Dns.GetHostName |> Dns.GetHostEntry
        let ipAddr = ipHostInfo.AddressList.[0]
        let endPoint = IPEndPoint (ipAddr, port)

        use listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        listener.Bind endPoint
        listener.Listen 10
        Console.WriteLine ("Server started and listening on " + string endPoint)

        while true do
            try
                let handler = listener.Accept ()
                Task.Run (handleConnection handler) |> ignore
            with
                | exn as ex -> Console.Error.WriteLine ex

    let client addr port =
        Console.Write "Message: "
        let msg = Console.ReadLine ()

        use client = new TcpClient ()
        client.Connect ((IPAddress.Parse addr), port)
        use stream = client.GetStream ()

        let buffer = Encoding.UTF8.GetBytes msg
        stream.Write (buffer, 0, buffer.Length)

        let rcvBuffer = Array.init 1024 (fun _ -> 0uy)
        stream.Read (rcvBuffer, 0, rcvBuffer.Length) |> ignore

        rcvBuffer |> Encoding.UTF8.GetString |> Console.WriteLine

    let main (args : string []) =
        match args.[0] with
        | "server" -> server (int args.[1])
        | "client" -> client args.[1] (int args.[2])
        | _ -> Console.WriteLine "Invalid command"
        ()
