﻿namespace DailyProgrammer.Hard

open System

module H6 =
    type Turn = Player | AI

    let player (state : int []) =
        Console.Write "Current game state: [ "
        Console.Write (state |> Array.map string |> String.concat ", ")
        Console.WriteLine " ]"
        Console.WriteLine "From which heap would you like to take?"
        Console.Write ("[0-" + string (state.Length - 1) + "] ")
        let i = int (Console.ReadLine ())
        Console.WriteLine "How many would you like to take?"
        Console.Write ("[1-" + string state.[i] + "] ")
        let n = int (Console.ReadLine ())
        Array.set state i (state.[i] - n)
        Console.WriteLine ()
        state

    let nimai (state : int []) =
        let s = state |> Array.reduce (^^^) // nim-sum
        if s = 0 then
            let i = state |> Array.findIndex (fun v -> v <> 0)
            let n = 1
            Array.set state i (state.[i] - n)
            Console.WriteLine ("AI takes " + string n + " from heap " + string i + ".")
            Console.WriteLine ()
        else
            let x = state |> Array.map (fun v -> v ^^^ s)
            let i = state |> Array.mapi (fun i v -> i, v) |> Array.find (fun (i, v) -> x.[i] < v) |> fst
            let n = state.[i] - x.[i]
            Array.set state i (state.[i] - n)
            Console.WriteLine ("AI takes " + string n + " from heap " + string i + ".")
            Console.WriteLine ()
        state

    let rec play turn state =
        if Array.exists (fun v -> v <> 0) state then
            match turn with
            | Player -> state |> player |> play AI
            | AI -> state |> nimai |> play Player
        else
            match turn with
            | Player -> Console.WriteLine "AI wins"
            | AI -> Console.WriteLine "Player wins"
            ()

    let main () =
        let initial = [| 3 ; 4 ; 5 |]
        play Player initial
        ()

module H7 =
    open RedditSharp
    let startBot user pass =
        // Uhhh.. on second though skipping this one
        ()

    let readPass () =
        let mutable pass = ""
        let mutable key = Console.ReadKey ()
        while key.Key <> ConsoleKey.Enter do
            if key.Key = ConsoleKey.Backspace then
                pass <- pass.[0..pass.Length-1]
            else
                pass <- pass + string key.KeyChar
                Console.Write "\b"
                key <- Console.ReadKey ()
        pass

    let main () =
        Console.Write "Username: "
        let username = Console.ReadLine ()
        Console.Write "Password: "
        let passwd = readPass ()
        startBot username passwd
        ()

module H8 =
    let fact = function
        | 0 -> 1
        | n when n > 0 -> Seq.reduce (*) (seq { 1..n })
        | _ -> raise (ArgumentException ())

    let comb n r =
        fact n / (fact r * fact (n-r))

    let strjoin sep (lst : string list) =
        String.Join (sep, lst)

    let rec printTri level maxLevel last =
        if level = 0 then
            Console.WriteLine "1"
            printTri (level+1) maxLevel [1]
        elif level < maxLevel then
            let current = 1 :: (last |> List.pairwise |> List.fold (fun s (a, b) -> (a + b) :: s) [1])
            let line = current |> List.map string |> strjoin " "
            Console.WriteLine line
            printTri (level+1) maxLevel current

    let main () =
        let input = Console.ReadLine ()
        if input = "print" then
            printTri 0 15 []
        else
            let parts = input.Split ',' |> Array.map int
            let out = comb (parts.[0] - 1) (parts.[1] - 1)
            Console.WriteLine out
        ()

module H9 =
    let strlast (arr : string) =
        arr.[arr.Length-1]

    let groupCount (str : string) =
        str.ToCharArray ()
        |> Array.map (fun c -> int (string c))
        |> Array.pairwise
        |> Array.mapi (fun i (v1, v2) -> i+1, (v1, v2))
        |> Array.filter (fun (i, (v1, v2)) -> v1 <> v2)
        |> Array.fold (fun s (i, (v1, v2)) -> (i, v1) :: s) [0, 0]
        |> (fun x y -> x :: y) (str.Length, int (string (strlast str)))
        |> List.rev
        |> List.pairwise
        |> List.map (fun ((v1, v2), (w1, w2)) -> (w1 - v1), w2)
        |> List.map (fun (count, v) -> (string count) + (string v))
        |> String.concat ""

    let groupCount2 (str : string) =
        let rec loop (s : string) acc =
            if s.Length = 0 then
                acc
            else
                let fch = s.[0]
                let l = s.ToCharArray ()
                        |> Array.takeWhile (fun c -> c = fch)
                        |> Array.length
                loop s.[l..] (acc + (string l) + (string fch))
        loop str ""

    let rec printLine i max (line : string) =
        Console.WriteLine line
        if i <> max then
            printLine (i+1) max (groupCount2 line)

    let main () =
        printLine 1 10 "1"
        ()

module H10 =
    open System.IO

    let rec hangman (word : string) (strikes : int) (letters : Set<char>) =
        Console.WriteLine ()
        if strikes = 0 then
            Console.WriteLine "Oh no, you lose!"
            Console.WriteLine ("The word was \"" + word + "\".")
        elif Array.forall (fun c -> letters.Contains c) (word.ToCharArray ()) then
            Console.WriteLine ("You guessed the word \"" + word + "\"!")
        else
            word.ToCharArray () |> Array.map (fun c -> if letters.Contains c then string c + " " else "_ ") |> Array.iter Console.Write
            Console.WriteLine ()
            letters |> Seq.sort |> Seq.iter Console.Write
            Console.WriteLine ()
            Console.WriteLine ("Strikes remaining: " + string strikes)
            Console.Write "Your guess: "
            let guess = (Console.ReadLine ()).[0]
            if letters.Contains guess then
                Console.WriteLine "You already guessed that!"
                hangman word strikes letters
            elif Array.exists (fun c -> c = guess) (word.ToCharArray ()) then
                hangman word strikes (letters.Add guess)
            else
                hangman word (strikes-1) (letters.Add guess)

    let main () =
        let wordlist = File.ReadAllLines "Hard/wordlist2.txt"
        let random = Random()
        hangman wordlist.[random.Next wordlist.Length] 8 Set.empty
        ()
