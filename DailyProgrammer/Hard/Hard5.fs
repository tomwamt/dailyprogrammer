﻿namespace DailyProgrammer.Hard

open System

module H21 =
    open System.IO

    let rec topoSort adjList =
        if Map.isEmpty adjList then
            []
        else
            let a = Map.findKey (fun _ s -> Set.isEmpty s) adjList
            let rst = adjList |> Map.map (fun _ s -> Set.remove a s) |> Map.remove a
            a :: topoSort rst

    let main args =
        let edges = File.ReadAllLines "Hard/smart.txt" |> Array.map (fun line ->
            let parts = line.Split ([|" : "|], StringSplitOptions.None)
            parts.[0], parts.[1]
        )
        let verts = Set.union
                        (edges |> Array.map fst |> Set.ofArray)
                        (edges |> Array.map snd |> Set.ofArray)
        let adjList = verts
                      |> Set.map (fun v ->
                          let adj = edges
                                    |> Array.filter (fun (f, _) -> f = v)
                                    |> Array.map snd
                                    |> Set.ofArray
                          v, adj
                      )
                      |> Map.ofSeq
        topoSort adjList
        |> List.rev
        |> List.iter Console.WriteLine
        ()

module H22 =
    open System.IO

    let rand = Random ()

    let mazeGen size =
        let rec mazeGenH (left, top, right, bot) nodiv = // splits horizontally
            if left = right then // 1 wide
                Set.empty
            elif left+1 = right then // 2 wide - no div
                if top+1 = bot then // 2*2 square
                    Set.empty
                else
                    mazeGenV (left, top, right, bot) 0
            elif left+2 = right && nodiv = left+1 then
                mazeGenV (left, top, right, bot) 0
            else
                let div = seq { while true do yield rand.Next(left+1, right) } |> Seq.find (fun x -> x <> nodiv)
                let hole = rand.Next(top, bot+1)
                Set.union
                    (Set.ofSeq (seq { for y in top..bot do if y <> hole then yield (div, y) }))
                    (Set.union
                        (mazeGenV (left, top, div-1, bot) hole)
                        (mazeGenV (div+1, top, right, bot) hole))
        and mazeGenV (left, top, right, bot) nodiv =
            if top = bot then
                Set.empty
            elif top+1 = bot then
                mazeGenH (left, top, right, bot) 0
            elif top+2 = bot && nodiv = top+1 then
                mazeGenH (left, top, right, bot) 0
            else
                let div = seq { while true do yield rand.Next(top+1, bot) } |> Seq.find (fun x -> x <> nodiv)
                let hole = rand.Next(left, right+1)
                Set.union
                    (Set.ofSeq (seq { for x in left..right do if x <> hole then yield (x, div) }))
                    (Set.union
                        (mazeGenH (left, top, right, div-1) hole)
                        (mazeGenH (left, div+1, right, bot) hole))

        mazeGenH (1, 1, size, size)

    let main (args : string []) =
        let s = int args.[0]
        let m = mazeGen s 0
        use file = new StreamWriter (args.[1])
        file.Write (string s + "\n")
        m |> Set.iter (fun (x,y) -> file.Write (string x + "," + string y + "\n"))
        ()

module H23 =
    open System.IO

    type CharCategory =
        | LowerAlpha
        | UpperAlpha
        | Numeral
        | Ascii
        | AsciiReverse
        | AsciiIgnoreCase
        | AsciiIgnoreCaseReverse

    type Ordering = Greater | Equal | Less

    let catMatch (a : char) cat  =
        match cat with
        | LowerAlpha -> int 'a' <= int a && int a <= int 'z'
        | UpperAlpha -> int 'A' <= int a && int a <= int 'Z'
        | Numeral -> int '0' <= int a && int a <= int '9'
        | _ -> true 

    let makeCf (descr : string) (ax : char, bx : char) =
        let catOrder =
            descr.Split ' '
            |> Array.map (function
                | "a-z" -> LowerAlpha
                | "A-Z" -> UpperAlpha
                | "0-9" -> Numeral
                | "ASCII-order" -> Ascii
                | "ASCII-order-ignore-case" -> AsciiIgnoreCase
                | "reverse-ASCII-order" -> AsciiReverse
                | "reverse-ASCII-order-ignore-case" -> AsciiIgnoreCaseReverse
                | _ -> failwith "Invalid notation"
            )

        let ai = Array.findIndex (catMatch ax) catOrder
        let bi = Array.findIndex (catMatch bx) catOrder
        if ai < bi then Less
        elif ai > bi then Greater
        else
            match catOrder.[ai] with
            | AsciiReverse ->
                if ax > bx then Less
                elif ax < bx then Greater
                else Equal
            | AsciiIgnoreCase ->
                if Char.ToLower ax < Char.ToLower bx then Less
                elif Char.ToLower ax > Char.ToLower bx then Greater
                else Equal
            | AsciiIgnoreCaseReverse ->
                if Char.ToLower ax > Char.ToLower bx then Less
                elif Char.ToLower ax < Char.ToLower bx then Greater
                else Equal
            | _ ->
                if ax < bx then Less
                elif ax > bx then Greater
                else Equal

    let arrTryFindi pred array =
        let rec loop i =
            if i = Array.length array then None
            elif pred i array.[i] then Some i
            else loop (i+1)

        loop 0

    let comp (cfs : (char * char -> Ordering) []) a b =
        let al = Array.length a
        let bl = Array.length b
        let len = (min al bl) - 1
        match Array.zip a.[0..len] b.[0..len] |> arrTryFindi (fun i x -> cfs.[i % cfs.Length] x <> Equal) with
        | Some i ->
            let cf = cfs.[i % cfs.Length]
            cf (a.[i], b.[i])
        | None ->
            if al < bl then
                Less
            elif al > bl then
                Greater
            else
                Equal

    let merge cmp (l1 : 'a list) (l2 : 'a list) =
        let rec loop ll1 ll2 acc =
            if ll1 = [] then List.append (List.rev acc) ll2
            elif ll2 = [] then List.append (List.rev acc) ll1
            else
                match cmp ll1.Head ll2.Head with
                | Less -> loop ll1.Tail ll2 (ll1.Head :: acc)
                | _ -> loop ll1 ll2.Tail (ll2.Head :: acc)

        loop l1 l2 []

    let mergesort merger l =
        let rec ms li =
            if List.length li <= 1 then li
            else
                let [left; right] = List.splitInto 2 li
                merger (ms left) (ms right)

        ms l

    let main (args : string []) =
        let lines = File.ReadAllLines args.[0]
        let n = int lines.[0]
        let cfs = lines.[1..n] |> Array.map makeCf

        lines.[n+1..]
        |> List.ofArray
        |> List.map (fun s -> s.ToCharArray())
        |> mergesort (merge (comp cfs))
        |> List.map String
        |> List.iter Console.WriteLine
        ()

module H25 =
    let r = Random()

    let conflicts (state : (int * int) []) x1 =
        let y1 = snd state.[x1]
        state
        |> Seq.ofArray
        |> Seq.filter (fun (x2, y2) -> x1 <> x2 && (y1 = y2 || (x1-y1) = (x2-y2) || (x1+y1) = (x2+y2)))
        |> Seq.length

    let solved state =
        Array.forall (fst >> conflicts state >> ((=) 0)) state

    let seqMinByRandom projection source =
        let min = 
            Seq.fold (fun mins elem ->
                let p = projection elem
                match mins with
                | [] -> [elem]
                | head :: tail ->
                    let ph = projection head
                    if p < ph then
                        [elem]
                    elif p = ph then
                        elem :: mins
                    else
                        mins
            ) [] source
        List.item (r.Next min.Length) min

    let rec minimizeConflicts iters rand state =
        if iters = 0 then
            false, state
        elif solved state then
            true, state
        else
            let m = state.Length-1
            let x = Seq.find (conflicts state >> ((<) 0)) rand
            let y' = 
                seq { 0..m }
                |> Seq.filter (fun y -> y <> x && (y+x) <> m)
                |> seqMinByRandom (fun y ->
                    let newState = Array.map (fun (x1, y1) -> if x = x1 then (x, y) else (x1, y1)) state
                    conflicts newState x)
            state |> Array.map (fun (x1, y1) -> if x = x1 then (x, y') else (x1, y1)) |> minimizeConflicts (iters-1) rand

    let minConflictsSolve n =
        let rand =
            seq {
                while true do
                    yield r.Next n
            }
        let init = Array.init n (fun x -> Seq.find (fun y -> y <> x && (x+y) <> (n-1)) rand) |> Array.indexed
        minimizeConflicts 200000 rand init

    let main (args : string []) =
        let n = int args.[0]
        let success, sol = minConflictsSolve n
        Console.WriteLine success
        sol
        |> Seq.ofArray
        |> Seq.map (fun (x, y) -> y, x)
        |> Seq.sortBy fst
        |> Seq.iter (fun (y, x) ->
            Console.WriteLine ((String.replicate x "_ ") + "Q " + (String.replicate (n-x-1) "_ "))
        )
        ()
