﻿namespace DailyProgrammer.Hard

open System

module H16 =
    open System.Collections.Generic

    let rec sieve i (table : Dictionary<int, Set<int>>) =
        if table.ContainsKey i then
            table.[i] |> Set.iter (fun p ->
                let n = i + 2*p
                if table.ContainsKey n then
                    table.[n] <- table.[n].Add (p)
                else
                    table.[n] <- Set.singleton p
            )

            sieve (i+2) table
        else
            table.[i*i] <- Set.singleton i
            (i, table)

    let main (args : string []) =
        let n = int args.[0]

        let primeGen (i, table) =
            let p, table' = sieve i table
            Some (p, (p+2, table'))

        Seq.append
            (Seq.singleton 2)
            (Seq.unfold primeGen (3, Dictionary ()))
        |> Seq.take n
        |> Seq.iter ignore
        //|> Seq.iter Console.WriteLine
        ()

module H17 =
    // Question was deleted, so this is revese-engineered
    // from the submitted solutions & discussion.
    // Create a tic-tac-toe AI
    let winrwd = 10
    let drawrwd = 0

    let size = 3

    let checkwin (player : char) (state : char [,]) =
        Seq.exists (fun x ->
            Seq.forall (fun y -> state.[x,y] = player) (seq { 0..(size-1) }) ||
            Seq.forall (fun y -> state.[y,x] = player) (seq { 0..(size-1) })
        ) (seq { 0..(size-1) }) ||
        Seq.forall (fun d -> state.[d,d] = player) (seq { 0..(size-1) }) ||
        Seq.forall (fun d -> state.[d,size-d-1] = player) (seq { 0..(size-1) })

    let checkdraw (state : char [,]) =
        Seq.allPairs
            (seq { 0..(size-1) })
            (seq { 0..(size-1) })
        |> Seq.exists (fun (x, y) -> state.[x,y] = ' ')
        |> not

    let makeMove (player : char) (x, y) (state : char [,]) =
        let newState = Array2D.copy state
        newState.[x, y] <- player
        newState

    let rec treemax state =
        if checkwin 'X' state then
            winrwd, (-1, -1)
        elif checkwin 'O' state then
            -winrwd, (-1, -1)
        elif checkdraw state then
            drawrwd, (-1, -1)
        else
            Seq.allPairs
                (seq { 0..(size-1) })
                (seq { 0..(size-1) })
            |> Seq.filter (fun (x, y) -> state.[x,y] = ' ')
            |> Seq.map (fun c ->
                let r, _ = state |> makeMove 'X' c |> treemin
                r, c
            )
            |> Seq.maxBy fst
    and treemin state =
        if checkwin 'X' state then
            winrwd, (-1, -1)
        elif checkwin 'O' state then
            -winrwd, (-1, -1)
        elif checkdraw state then
            drawrwd, (-1, -1)
        else
            Seq.allPairs
                (seq { 0..(size-1) })
                (seq { 0..(size-1) })
            |> Seq.filter (fun (x, y) -> state.[x,y] = ' ')
            |> Seq.map (fun c ->
                let r, _ = state |> makeMove 'O' c |> treemax
                r, c
            )
            |> Seq.minBy fst

    let printState (state : char [,]) =
        for y = 0 to size-1 do
            for x = 0 to size-1 do
                Console.Write (string state.[x,y] + " ")
            Console.WriteLine ()

    let tttai state =
        Console.WriteLine "X Turn"
        printState state
        let _, c = treemax state
        Console.WriteLine c
        makeMove 'X' c state

    let ttthuman state =
        Console.WriteLine "O Turn"
        printState state
        
        Console.WriteLine "Move: "
        let input = Console.ReadLine ()
        let cs = input.Split ' ' |> Array.map int
        makeMove 'O' (cs.[0], cs.[1]) state

    type Player = Human | AI

    let rec playttt turn state =
        if checkwin 'X' state then
            Console.WriteLine "X wins"
            printState state
        elif checkwin 'O' state then
            Console.WriteLine "O wins" // lol never gonna happen
            printState state
        elif checkdraw state then
            Console.WriteLine "Draw"
            printState state
        else
            match turn with
            | AI -> state |> tttai |> playttt Human
            | Human -> state |> ttthuman |> playttt AI

    let main args =
        playttt AI (Array2D.init size size (fun x y -> ' '))
        ()

module H18 =
    open System.Drawing

    let spiral = function
        | 0 -> [(0, 0)]
        | level -> [(level-1, -level+2) ; (level-1, level) ; (-level, level) ;  (-level, -level)]

    let main (args : string []) =
        let size = int args.[0]
        let ox, oy = (size/2, size/2)

        let points = seq { 0..2..size/2 }
                     |> Seq.collect spiral
                     |> Seq.map (fun (x, y) -> Point (x+ox, y+oy))
                     |> Array.ofSeq

        use img = new Bitmap (size, size)
        use gr = Graphics.FromImage img
        gr.DrawLines (Pens.White, points)
        img.Save "out.png"
        ()

module H19 =
    open System.IO
    open System.Text.RegularExpressions
    open System.Collections.Generic

    let strjoin (sep : string) (strs : seq<string>) = String.Join(sep, strs)

    let strtolower (str : string) =
        str.ToLower ()

    let main args =
        let pages = File.ReadAllLines "Easy/sherlock.txt"
                    |> Array.filter (fun line -> not (Regex.IsMatch(line, "(ADVENTURE [XVI]+.(\W[A-Z]+)+)|([XVI]+. THE ADVENTURE OF(\W[A-Z]+)+)")))
                    |> Array.map (fun line -> line + " ")
                    |> Array.filter (fun line -> not (Regex.IsMatch(line, "[XVI]+. ")))
                    |> Array.chunkBySize 40
                    |> Array.map String.Concat
                    |> Array.indexed

        let index = Dictionary<string, int list> ()
        pages
        |> Array.iter (fun (i, page) ->
            let words = Regex.Split(page, "\W+") |> Array.distinct |> Array.map strtolower
            words |> Array.iter (fun word ->
                if index.ContainsKey word then
                    index.[word] <- i :: index.[word]
                else
                    index.[word] <- [i]
            )
        )

        index
        |> Seq.map (fun kvp -> (kvp.Key, List.sort kvp.Value))
        |> Seq.filter (fun (_, pages) -> pages.Length < 100)
        |> Seq.sortBy fst
        |> Seq.iter (fun (word, pages) ->
            Console.Write (word + ": ")
            pages |> List.map (fun p -> string (p+1)) |> strjoin ", " |> Console.WriteLine
        )
        ()

module H20 =
    open System.Threading
    open System.Windows.Forms

    let twoHours = TimeSpan (2, 0, 0)

    let rec annoy () =
        Thread.Sleep twoHours
        MessageBox.Show "Stop procrastinating!" |> ignore
        annoy ()

    let main args =
        annoy ()
        ()
