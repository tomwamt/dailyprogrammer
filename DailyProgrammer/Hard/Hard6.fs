﻿namespace DailyProgrammer.Hard

open System

module H27 =
    // going to be lazy for this one
    let rec manipulate (time : DateTime) =
        Console.WriteLine ("Current date & time: " + string time)
        Console.Write "Enter an adjustment: "
        let adj = Console.ReadLine().Split ' '
        let amt = float adj.[0]
        let unt = if adj.[1].EndsWith "s" then adj.[1] else adj.[1] + "s"
        match unt with
        | "milliseconds" -> manipulate (time.AddMilliseconds amt)
        | "seconds" -> manipulate (time.AddSeconds amt)
        | "minutes" -> manipulate (time.AddMinutes amt)
        | "hours" -> manipulate (time.AddHours amt)
        | "days" -> manipulate (time.AddDays amt)
        | "weeks" -> manipulate (time.AddDays (amt*7.0))
        | "months" -> manipulate (time.AddMonths (int amt))
        | "years" -> manipulate (time.AddYears (int amt))
        | _ -> ()

    let main (args : string []) =
        Console.Write "Enter a date & time (YYYY-MM-DDThh:mm:ss or 'now'): "
        let dts = Console.ReadLine()
        let dt = if dts.ToLower() = "now" then DateTime.Now else DateTime.Parse dts
        manipulate dt
        ()

module H28 =
    open System.Collections.Generic
    open System.Net.Http
    open System.IO

    let main (args : string []) =
        let apikey = (File.ReadAllLines "Hard/pastebin_key.txt").[0]
        let pastebinData = File.ReadAllText args.[0]

        let data = Dictionary<string, string>()
        data.["api_dev_key"] <- apikey
        data.["api_option"] <- "paste"
        data.["api_paste_code"] <- pastebinData
        data.["api_paste_expire_date"] <- "10M"

        use client = new HttpClient()
        use content = new FormUrlEncodedContent(data)
        let resp = client.PostAsync("https://pastebin.com/api/api_post.php", content) |> Async.AwaitTask |> Async.RunSynchronously
        let respString = resp.Content.ReadAsStringAsync() |> Async.AwaitTask |> Async.RunSynchronously
        Console.WriteLine respString
        ()

module H29 =
    open System.Drawing

    let drawLine (x1 : int, y1 : int) (x2 : int, y2 : int) (width : int) (color : Color) (img : Bitmap) =
        let xmin = (min x1 x2) - width
        let xmax = (max x1 x2) + width
        let ymin = (min y1 y2) - width
        let ymax = (max y1 y2) + width
        Seq.allPairs (seq { xmin..xmax }) (seq { ymin..ymax })
        |> Seq.iter (fun (x0, y0) ->
            let dp = sqrt(float(y2-y1) ** 2.0 + float(x2-x1) ** 2.0)
            let d =
                if dp = 0.0 then
                    sqrt(float(y0-y1) ** 2.0 + float(x0-x1) ** 2.0)
                else
                    float (abs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)) / dp

            if d <= float(width)/2.0 then
                img.SetPixel(x0, y0, color)
        )
        img

    let main (args : string []) =
        use img = new Bitmap(200, 200)
        img
        |> drawLine (10, 20) (50, 150) 1 Color.White
        |> drawLine (170, 50) (60, 70) 4 Color.Red
        |> (fun i -> i.Save "out.png")
        ()

module H30 =
    let matmodmult (a : bigint [][]) (b : bigint [][]) (c : bigint) =
        [|
            [|(a.[0].[0] * b.[0].[0] + a.[0].[1] * b.[1].[0]) % c; (a.[0].[0] * b.[0].[1] + a.[0].[1] * b.[1].[1]) % c|]
            [|(a.[1].[0] * b.[0].[0] + a.[1].[1] * b.[1].[0]) % c; (a.[1].[0] * b.[0].[1] + a.[1].[1] * b.[1].[1]) % c|]
        |]

    let rec matmodpow (a : bigint [][]) (n : int64) (m : bigint) =
        match n with
        | 0L -> [|[|bigint.One; bigint.Zero|]; [|bigint.Zero; bigint.One|]|]
        | 1L -> a
        | n ->
            if n % 2L = 0L then
                let a' = matmodpow a (n/2L) m
                matmodmult a' a' m
            else
                let a' = matmodpow a (n-1L) m
                matmodmult a' a m

    let main args =
        let A = [|[|bigint.One; bigint.One|]; [|bigint.One; bigint.Zero|]|]
        let B = matmodpow A (pown 10L 18) (bigint 100000000)
        Console.WriteLine B.[1].[0]
        ()
