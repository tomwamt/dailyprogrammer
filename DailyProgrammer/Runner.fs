﻿namespace DailyProgrammer

module Runner = 
    [<EntryPoint>]
    let main args = 
        Medium.M29.main args
        0
