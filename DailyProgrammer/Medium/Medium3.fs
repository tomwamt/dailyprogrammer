﻿namespace DailyProgrammer.Medium

open System

module M11 =
    let table =
        dict [
            '1', '1'
            '2', '2'
            '3', 'x'
            '4', 'x'
            '5', '5'
            '6', '9'
            '7', 'x'
            '8', '8'
            '9', '6'
            '0', '0'
        ]

    let isUpsideUp (n : int) =
        let cha = (string n).ToCharArray ()
        (Array.map (fun c -> table.[c]) cha) = (Array.rev cha)

    let main () =
        seq { 1961..10000 } |> Seq.filter isUpsideUp |> Seq.iter Console.WriteLine
        ()

module M12 =
    let factor n =
        let rec loop n acc =
            if n = 1 then
                acc
            else
                let f = Seq.find (fun f -> n % f = 0) (seq { 2..n })
                loop (n/f) (f :: acc)
        loop n []

    let strjoin sep (lst : string seq) =
        String.Join (sep, lst)

    let main (args : string []) =
        args.[0] |> int |> factor |> List.map string |> strjoin " * " |> Console.WriteLine
        ()

module M13 =
    let main args =
        Console.Write "Input: "
        let input = Console.ReadLine ()
        let reversed = input.ToCharArray () |> Array.rev |> String
        Console.WriteLine reversed // Use redirection if you want it in a file...
        ()

module M14 =
    let seive n =
        // inefficient, but it works...
        Seq.allPairs (seq { 1..n }) (seq { 1..n }) |> Seq.exists (fun (i, j) -> i <= j && (i + j + 2*i*j = n)) |> not

    let main (args : string []) =
        Console.WriteLine 2
        (seq { 1..(int args.[0])}) |> Seq.filter seive |> Seq.map (fun x -> 2*x + 1) |> Seq.iter Console.WriteLine
        ()

module M15 =
    open MathNet.Numerics.LinearAlgebra

    let size = 30 // 30
    let sizeSq = size * size

    let chance (x, y) =
        if x = 1 || x = size then
            if y = 1 || y = size then
                1.0/2.0
            else
                1.0/3.0
        else
            if y = 1 || y = size then
                1.0/3.0
            else
                1.0/4.0

    let cells = [| for x in 1..size do for y in 1..size -> (x, y) |]

    let isNeighbor (x1, y1) (x2, y2) =
        (abs (x1-x2) + abs (y1-y2)) = 1

    let matBuild i j =
        let src = cells.[j]
        let dest = cells.[i]
        if isNeighbor src dest then
            chance src
        else
            0.0

    let ( ** ) (mat : Matrix<'T>) (p : int) =
        mat.Power p

    let main args =
        let markov = Matrix<float>.Build.Dense (sizeSq, sizeSq, matBuild)
        let ones = Vector<float>.Build.Dense (sizeSq, 1.0)

        markov ** 50
        |> Matrix.mapCols (fun j col -> ones - col)
        |> Matrix.reduceCols (.*)
        |> Vector.sum
        |> Console.WriteLine
        ()
