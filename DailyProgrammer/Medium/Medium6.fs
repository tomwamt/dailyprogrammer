﻿namespace DailyProgrammer.Medium

open System

module M26 =
    type Employee = { name : string; age : int; pay : float }

    let listEmployees emps =
        Map.iter (fun _ (emp : Employee) ->
            Console.WriteLine (emp.name + " Age: " + (string emp.age) + " Pay: " + (string emp.pay))
        ) emps
        emps

    let addEmployee emps =
        Console.Write "Enter name: "
        let name = Console.ReadLine ()
        if Map.containsKey name emps then
            Console.WriteLine (name + " already exists")
            emps
        else
            Console.Write "Enter age: "
            let age = int (Console.ReadLine ())
            Console.Write "Enter pay: "
            let pay = float (Console.ReadLine ())
            Map.add name { name=name; age=age; pay=pay } emps

    let removeEmployee emps =
        Console.Write "Enter name: "
        let name = Console.ReadLine ()
        if Map.containsKey name emps then
            Map.remove name emps
        else
            Console.WriteLine (name + " does not exist")
            emps

    let editEmployee emps =
        Console.Write "Enter name: "
        let name = Console.ReadLine ()
        if Map.containsKey name emps then
            let mutable emp = Map.find name emps
            Console.Write "Enter new age, or blank for same: "
            let ages = Console.ReadLine ()
            if ages <> "" then
                emp <- { emp with age=(int ages) }
            Console.Write "Enter new pay, or blank for same: "
            let pays = Console.ReadLine ()
            if pays <> "" then
                emp <- { emp with pay=(float pays) }
            Map.add name emp emps
        else
            Console.WriteLine (name + " does not exist")
            emps

    let rec menu (emps : Map<string, Employee>) =
        Console.WriteLine "Enter action (list, add, remove, edit, quit):"
        let action = Console.ReadLine ()
        match action with
        | "list" -> emps |> listEmployees |> menu
        | "add" -> emps |> addEmployee |> menu
        | "remove" -> emps |> removeEmployee |> menu
        | "edit" -> emps |> editEmployee |> menu
        | "quit" -> ()
        | _ -> menu emps

    let main (args : string []) =
        menu Map.empty
        ()

module M27 =
    let main (args : string []) =
        let year = int args.[0]
        let spd = DateTime(year, 3, 17)
        Console.WriteLine spd.DayOfWeek
        ()

module M28 =
    let big1 = bigint 1
    let big2 = bigint 2
    let big6 = bigint 6

    let tetra (n : bigint) =
        (n * (n + big1)*(n + big2)) / big6

    let main (args : string []) =
        let target = bigint.Parse args.[0]
        Seq.initInfinite bigint |> Seq.find (tetra >> (=) target) |> Console.WriteLine
        ()

module M29 =
    open System.IO
    open System.Net
    open System.Text

    let handleRequest (request : HttpListenerRequest) =
        match request.Url.Segments with
        | [| "/" |] ->
            let respData = File.ReadAllText "Medium/m29.html"
            (200, respData)
        | [| "/"; "post" |] ->
            use reader = new StreamReader(request.InputStream)
            let filename = reader.ReadLine()
            let contents = reader.ReadToEnd()
            File.WriteAllText(filename, contents)
            (200, "")
        | _ -> (404, "Not Found")

    let main (args : string []) =
        use listener = new HttpListener()
        listener.Prefixes.Add("http://*:8080/")
        listener.Start()
        Console.WriteLine "Listening..."
        while true do
            let context = listener.GetContext()
            let status, respStr = handleRequest context.Request
            let resp = Encoding.UTF8.GetBytes(respStr)
            context.Response.StatusCode <- status
            context.Response.ContentLength64 <- int64 resp.Length
            context.Response.OutputStream.Write(resp, 0, resp.Length)
            context.Response.Close()
        ()

module M30 =
    let parseChar c =
        Array.singleton c |> String |> int

    let charArray (s : string) = s.ToCharArray()

    let luhn n =
        n
        |> charArray
        |> Array.map parseChar
        |> Array.rev
        |> Array.fold (fun (state, dbl) d ->
            let x = if dbl then 2*d else d
            (state + string x, not dbl)
        ) ("", false)
        |> fst
        |> charArray
        |> Array.map parseChar
        |> Array.reduce (+)

    let verify n =
        luhn n % 10 = 0

    let getCheck n =
        (10 - (luhn n % 10)) % 10

    let main (args : string []) =
        let n = args.[1]
        match args.[0] with
        | "validate" ->
            Console.WriteLine (if verify n then "Valid" else "Invalid")
        | "checkdigit" ->
            Console.WriteLine (getCheck n)
        | _ ->
            Console.WriteLine "???"
        ()
