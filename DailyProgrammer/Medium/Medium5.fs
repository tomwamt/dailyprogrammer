﻿namespace DailyProgrammer.Medium

open System

module M21 =
    open MathNet.Numerics.LinearAlgebra

    let rec directHaar (arr : float list) =
        if arr.Length = 1 then
            arr
        else
            arr
            |> List.chunkBySize 2
            |> List.map (fun [a; b] -> (a+b)/2.0, (a-b)/2.0)
            |> List.unzip
            |> (fun (ap, de) -> List.append (directHaar ap) de)

    let matrixHaar (arr : float list) =
        let v = vector arr
        let W = matrix [[0.125;  0.125;  0.25;  0.0;  0.5;  0.0;  0.0;  0.0]
                        [0.125;  0.125;  0.25;  0.0; -0.5;  0.0;  0.0;  0.0]
                        [0.125;  0.125; -0.25;  0.0;  0.0;  0.5;  0.0;  0.0]
                        [0.125;  0.125; -0.25;  0.0;  0.0; -0.5;  0.0;  0.0]
                        [0.125; -0.125;  0.0;   0.25; 0.0;  0.0;  0.5;  0.0]
                        [0.125; -0.125;  0.0;   0.25; 0.0;  0.0; -0.5;  0.0]
                        [0.125; -0.125;  0.0;  -0.25; 0.0;  0.0;  0.0;  0.5]
                        [0.125; -0.125;  0.0;  -0.25; 0.0;  0.0;  0.0; -0.5]]
        (W.Transpose())*v |> Vector.toList

    let main args =
        let a = [ 420.0; 680.0; 448.0; 708.0; 1260.0; 1420.0; 1600.0; 1600.0 ]
        let b = directHaar a
        let c = matrixHaar a
        printfn "%A" a
        printfn "%A" b
        printfn "%A" c
        ()

module M22 =
    open System.IO

    type MazeCell = Empty | Wall | Goal
    type MazeState = { maze : MazeCell [,]; mutable pos : int * int; mutable win : bool }

    let printState (state : MazeState) =
        let m = state.maze.GetLength 0 - 1
        for y in 0..m do 
            for x in 0..m do
                if (x, y) = state.pos then
                    Console.Write "<>"
                else
                    let c =
                        match state.maze.[x, y] with
                        | Empty -> "  "
                        | Wall -> "##"
                        | Goal -> "=="
                    Console.Write c
            Console.WriteLine ()

    let handleInput state =
        let x, y = state.pos
        let key = Console.ReadKey ()
        let nx, ny =
            match key.Key with
            | ConsoleKey.UpArrow -> (x, y-1)
            | ConsoleKey.DownArrow -> (x, y+1)
            | ConsoleKey.LeftArrow -> (x-1, y)
            | ConsoleKey.RightArrow -> (x+1, y)
            | ConsoleKey.Escape ->
                Console.WriteLine ()
                Environment.Exit 0
                (0, 0)
            | _ -> (x, y)

        // DIRTY IMPURE HACK!!!1!1one
        if state.maze.[nx, ny] <> Wall then
            state.pos <- (nx, ny) 
            if state.maze.[nx, ny] = Goal then
                state.win <- true

        state

    let rec play state =
        Console.Clear ()
        printState state
        if not state.win then
            state |> handleInput |> play
        else
            Console.WriteLine "A winner is you!"

    let main (args : string []) =
        let lines = File.ReadAllLines args.[0]
        let size = int lines.[0] + 2
        let walls =
            lines.[1..]
            |> Array.map (fun line ->
                let parts = line.Split ','
                (int parts.[0]), (int parts.[1])
            )
            |> Set.ofArray

        let start = (1, 1)
        let goal = (size-2, size-2)
        let grid =
            Array2D.init size size
                (fun x y ->
                    match (x, y) with
                    | w when Set.contains w walls -> Wall
                    | g when g = goal -> Goal
                    | 0, _ | _, 0 -> Wall
                    | s, _ | _, s when s = size-1 -> Wall
                    | _ -> Empty
                )
        let state = { maze = grid; pos = start; win = false }
        play state
        ()

module M23 =
    let sizes = [6; 9; 20]

    let rec isMcn n =
        lazy(
            if n < 0 then false
            elif n = 0 then true
            else List.exists (fun s -> isMcn(n - s).Value) sizes
        )

    let main args =
        let a = sizes.Head
        let x =
            seq {
                let flags = Array.replicate a true
                let mutable i = 0
                while Array.exists id flags do
                    if not (isMcn(i).Value) then
                        yield i
                    else
                        flags.[i%a] <- false
                    i <- i + 1
            }
        Seq.iter (printf "%d ") x
        printfn ""
        ()

module M24 =
    open MathNet.Numerics

    let pifolder pi i =
        let n = 4 * (pown -1 (i+1))
        let d = 2*i + 3
        pi + BigRational.FromIntFraction(n, d)

    let main args =
        let piseq = Seq.scan pifolder (BigRational.FromIntFraction(4, 1)) (Seq.initInfinite id)
        for pi in piseq do
            Console.WriteLine pi
            Console.ReadLine () |> ignore
        ()

module M25 =
    let main (args : string []) =
        Console.WriteLine(Convert.ToString(int args.[0], 2))
        ()
