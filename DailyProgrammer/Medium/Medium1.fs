﻿namespace DailyProgrammer.Medium

open System

module M1 = 
    let rec insert_into (e : int * string) (lst : list<int * string>) = 
        if lst.IsEmpty then
            e :: []
        elif fst e < fst lst.Head then
            e :: lst
        else
            lst.Head :: insert_into e lst.Tail

    let add_item schedule = 
        Console.Write("Enter an hour: ")
        let hour = Console.ReadLine() |> int
        Console.Write("Enter the event: ")
        let event = Console.ReadLine()
        insert_into (hour, event) schedule

    let show schedule =
        printfn "%A" schedule
        schedule

    let rec delete_from (e : int) (lst : list<int * string>) = 
        if lst.IsEmpty then
            lst
        elif e = fst lst.Head then
            lst.Tail
        else
            lst.Head :: delete_from e lst.Tail

    let delete_item schedule = 
        Console.Write("Enter the hour of the entry to delete: ")
        let hour = Console.ReadLine() |> int
        delete_from hour schedule

    let rec menu schedule = 
        Console.Write("Enter a command: ")
        let choice = Console.ReadLine()
        match choice with
        | "add" -> menu (add_item schedule)
        | "show" -> menu (show schedule)
        | "delete" -> menu (delete_item schedule)
        | _ -> schedule

    let main() = menu []

module M2 = 
    type Direction = 
        | Up
        | Down
        | Left
        | Right

    type Room(up, down, left, right) = 
        member public this.up : Room option = up;
        member public this.down : Room option = down;
        member public this.left : Room option = left;
        member public this.right : Room option = right;

module M3 = 
    let ctoi (c : char) =
        let i = int c
        if 97 <= i && i <= 122 then // a-z -> 0-25
            i - 97
        elif 48 <= i && i <= 57 then // 0-9 -> 26-35
            i - 48 + 26
        elif i = 32 then // space -> 36
            36
        else // invalid char
            raise (ArgumentException (string c))

    let itoc i =
        if i < 26 then
            char (i + 97)
        elif i < 36 then
            char (i - 26 + 48)
        else
            ' '

    let e c i =
        itoc ((ctoi c + (i * 2 + i)) % 37)

    let encrypt text =
        Seq.mapi (fun i c -> e c i) text |> String.Concat

    let d c i =
        itoc ((ctoi c + 370 - (i * 2 + i)) % 37)

    let decrypt text =
        Seq.mapi (fun i c -> d c i) text |> String.Concat

    let main () =
        Console.Write "Enter a message: "
        let msg = Console.ReadLine ()
        let emsg = encrypt msg
        Console.WriteLine emsg
        Console.WriteLine (decrypt emsg)
        ()

module M4 =
    type ParseNode(ch, children) =
        member this.ch : char = ch
        member this.children : ParseNode [] = Array.ofList children

        member this.cmp (l1 : ParseNode) (l2 : ParseNode) =
            if l1.children.Length = 0 && l2.children.Length = 0 then
                this.ch.CompareTo l2.ch
            else
                Array.compareWith this.cmp l1.children l2.children

        interface IComparable with
            member this.CompareTo other =
                Array.compareWith this.cmp this.children (other :?> ParseNode).children

    let parse (str : string) =
        let empty (j : int) = Set.empty.Add (j, ParseNode(char 0, []))
        let term (x : char) (j : int) =
            if j >= str.Length then
                Set.empty
            elif str.[j] = x then
                Set.empty.Add (j + 1, ParseNode(x, []))
            else
                Set.empty
        
        let (.+) (p : int -> Set<int * ParseNode>) (q : int -> Set<int * ParseNode>) =
            fun j -> Set.union (p j) (q j)
        let (.*) (p : int -> Set<int * ParseNode>) (q : int -> Set<int * ParseNode>) =
            let aq k n1 =
                q k |> Set.map (fun (i, n2) -> i, ParseNode(char 0, [n1; n2]))

            fun j -> Set.unionMany [ for k, node in (p j) -> aq k node ]

        // concatenation: like .* but more than 2
        let cat (ps : (int -> Set<int * ParseNode>) list) j =
            ps 
            |> List.fold (fun st p ->
                Set.unionMany (seq { for i, nodes in st -> Set.map (fun (k, node) -> k, node :: nodes) (p i) })
            ) (Set.empty.Add(j, []))
            |> Set.map (fun (k, nodes) -> k, ParseNode(char 0, List.rev nodes))

        // Kleen star operator *
        let star (p : int -> Set<int * ParseNode>) j =
            let rec starh (last : Set<int * ParseNode list>) =
                Set.union
                    last
                    (Set.unionMany (seq { for i, nodes in last -> p i |> Set.map (fun (k, node) -> k, node :: nodes) |> starh }))

            starh (Set.singleton (j, [])) |> Set.map (fun (k, nodes) -> k, ParseNode(char 0, List.rev nodes))

        // Optional operator ?
        let opt (p : int -> Set<int * ParseNode>) j =
            (p .+ empty) j

        let rec start j =
            (expr .* term '$') j
        and expr j =
            cat [product; star (addop .* product)] j
        and product j =
            cat [value; star (mulop .* value)] j
        and addop j =
            (term '+' .+ term '-') j
        and mulop j =
            (term '*' .+ term '/') j
        and value j =
            (number .+ paren) j
        and number j =
            cat [opt (term '-') ; numeral ; star numeral; opt (term '.' .* star numeral)] j
        and numeral j =
            (term '0' .+ term '1' .+ term '2'.+ term '3'.+ term '4'.+ term '5'.+ term '6'.+ term '7'.+ term '8'.+ term '9') j
        and paren j =
            cat [term '(' ; expr ; term ')'] j
        
        start 0

    let rec printTree (node : ParseNode) =
        Console.Write "{"
        if node.ch <> char 0 then
            Console.Write node.ch
        else
            node.children |> Array.iter printTree
        Console.Write "}"

    let eval (tree : ParseNode) =
        let rec start (node : ParseNode) =
            expr node.children.[0]
        and expr node =
            let s0 = product node.children.[0]
            node.children.[1].children
            |> Array.fold (fun s child ->
                if child.children.[0].ch = '+' then
                    s + product child.children.[1]
                else
                    s - product child.children.[1]
                ) s0
        and product node =
            let p0 = value node.children.[0]
            node.children.[1].children
            |> Array.fold (fun p child ->
                if child.children.[0].ch = '*' then
                    p * value child.children.[1]
                else
                    p / value child.children.[1]
                ) p0
        and value node =
            if node.children.[0].ch = '(' then // paren
                expr node.children.[1]
            else // number
                let v = string node.children.[1].ch + numeralstring node.children.[2] + decimalpart node.children.[3] |> float
                if node.children.[0].ch = '-' then
                    -v
                else
                    v
        and numeralstring node =
            node.children |> Array.map (fun child -> string child.ch) |> String.Concat
        and decimalpart node =
            if node.children.Length = 0 then
                String.Empty
            else
                string node.children.[0].ch + numeralstring node.children.[1]
        
        start tree

    let main () =
        Console.Write "Input: "
        let inp = Console.ReadLine () + "$"
        for _, node in parse inp do
            printTree node
            Console.WriteLine ()
            Console.WriteLine (eval node)
        ()

module M5 =
    let compare (word1 : string) (word2 : string) = 
        (word1 <> word2) && (String.Concat (Array.sort (word1.ToCharArray () )) = String.Concat (Array.sort (word2.ToCharArray () )))

    let getAnagram (word : string) (lst : string list) =
        try
            Some(lst |> List.find (compare word))
        with
        | _ -> None

    let rec anagramSearch (words : string list) =
        if words.IsEmpty then
            ()
        else
            let anagram = getAnagram words.Head words.Tail
            if anagram.IsSome then
                Console.WriteLine (words.Head + " " + anagram.Value)
            anagramSearch words.Tail

    let main () =
        let text = IO.File.ReadAllText "Medium/anagram.txt"
        let splitters = [|
            ' ' ; '\n' ; '\r' ; '\t' ; '.' ; ':' ; ',' ; '?'
            '(' ; ')' ; '"'
        |]
        let words = List.ofArray (text.Split (splitters, StringSplitOptions.RemoveEmptyEntries))
        anagramSearch words
        ()