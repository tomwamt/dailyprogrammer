﻿namespace DailyProgrammer.Medium

open System

module M16 =
    let rand = Random ()

    let d6 () = (rand.Next 6) + 1

    let rec point p =
        let roll = d6 () + d6 ()
        match roll with
        | 7 -> false
        | r when r = p -> true
        | _ -> point p

    let comeOut () =
        let roll = d6 () + d6 ()
        match roll with
        | 2 | 3 | 12 -> false
        | 7 | 11 -> true
        | _ -> point roll

    let main (args : string []) =
        let n = int args.[0]
        let results = Seq.init n (fun _ -> comeOut ()) |> Seq.countBy id |> dict
        Console.WriteLine ("Wins: " + string (float results.[true] / float n))
        Console.WriteLine ("Losses: " + string (float results.[false] / float n))
        ()

module M17 =
    // Question was deleted, so this is revese-engineered
    // from the submitted solutions & discussion.
    // Open a webpage and search the contents for a specific string.
    // Return the line(s) that that string appears on.
    open System.Net
    open System.Text.RegularExpressions

    let main (args : string []) =
        use client = new WebClient ()
        let data = client.DownloadString args.[0]
        let lines = data.Split '\n'
        lines |> Array.iteri (fun n line ->
            if Regex.IsMatch (line, args.[1]) then
                Console.WriteLine ("[Line " + string n + "] " + line)
        )
        ()

module M18 =
    open System.IO
    open System.Text.RegularExpressions

    let re = Regex "^([\w]+) ?(\(\[[A-Z]\][\w ]*(, \[[A-Z]\][\w ]*)+\))?:$"

    let emitTextInput name =
        "<input type=\"text\" name=\"" + name + "\"/>"

    let emitRadioOption name (value : string) =
        let v = value.[1..1].ToLower ()
        let full = value.ToCharArray () |> Array.filter (fun c -> c <> '[' && c <> ']') |> String
        "<input type=\"radio\" name=\"" + name + "\" value=\"" + v + "\"/> " + full

    let emitRadio name values =
        values |> Array.map (emitRadioOption name) |> String.Concat

    let emitSelectOption (value : string) =
        let v = value.[1..1].ToLower ()
        let full = value.ToCharArray () |> Array.filter (fun c -> c <> '[' && c <> ']') |> String
        "<option value=\"" + v + "\">" + full + "</option>"

    let emitSelect name values =
        "<select name=\"" + name + "\">" +
        (values |> Array.map emitSelectOption |> String.Concat) +
        "</select>"

    let emitField line =
        let mtch = re.Match line
        mtch.Groups.[1].Value + ": " +
        (if mtch.Groups.[2].Success then
            let len = mtch.Groups.[2].Value.Length
            let parts = mtch.Groups.[2].Value.[1..len-2].Split ([| ", " |], StringSplitOptions.None)
            if parts.Length < 5 then
                emitRadio (mtch.Groups.[1].Value.ToLower ()) parts
            else
                emitSelect (mtch.Groups.[1].Value.ToLower ()) parts
        else
            emitTextInput (mtch.Groups.[1].Value.ToLower ())
        ) +
        "<br/>"

    let parse (lines : string []) =
        "<html>" +
        "<body>" +
        "<form>" +
        (lines |> Array.map emitField |> String.Concat) +
        "<input type=\"submit\" value=\"Submit\"/>" +
        "</form>" +
        "</body>" +
        "</html>\n"

    let main (args : string []) =
        let output = File.ReadAllLines args.[0] |> Array.filter (fun s -> s <> "") |> parse
        File.WriteAllText (args.[0] + ".html", output)
        ()

module M19 =
    open System.IO
    open System.Text.RegularExpressions
    open System.Collections.Generic

    let main (args : string []) =
        let titles = List<string> ()
        let book = File.ReadAllLines "Easy/sherlock.txt"
                   |> Array.map (fun line ->
                       if Regex.IsMatch(line, "(ADVENTURE [XVI]+.(\W[A-Z]+)+)|([XVI]+. THE ADVENTURE OF(\W[A-Z]+)+)") then
                           titles.Add(line)
                           "xxxSPLITPOINTxxx "
                       else
                           line + " ")
                   |> Array.filter (fun line -> not (Regex.IsMatch (line, "[XVI]+. ")))
                   |> String.Concat

        let chapters = Regex.Split(book, "xxxSPLITPOINTxxx ")
                       
        chapters
        |> Array.skip 1
        |> Array.mapi (fun i chapt ->
            let words = Regex.Split(chapt, "\W+") |> Array.filter (fun word -> word <> "")
            (titles.[i], words.Length))
        |> Array.sortBy (fun (title, count) -> -count)
        |> Array.iter (fun (title, count) -> Console.WriteLine (title + " -- " + string count))
        ()


module M20 =
    // modifying to make more interesting
    let main args =
        Console.Write "Enter your birthday (MM/DD/YYYY): "
        let birth = DateTime.Parse (Console.ReadLine ())
        let age = DateTime.Now - birth
        Console.WriteLine "Your age in..."
        Console.WriteLine ("Years (approx): " + string (age.Days/365))
        Console.WriteLine ("Months (approx): " + string (age.Days/30))
        Console.WriteLine ("Days: " + (age.TotalDays |> int |> string))
        Console.WriteLine ("Hours: " + (age.TotalHours |> int |> string))
        Console.WriteLine ("Minutes: " + (age.TotalMinutes |> int |> string))
