﻿namespace DailyProgrammer.Medium

open System

// This one's really vague, even with the clarification
// Going with "any duplicated substring of length 4 or greater is removed from the string"
module M6 =
    open System.Text.RegularExpressions

    let rec scan text =
        let mtch = Regex.Match (text, ".*?(.{4,}).*?(\\1)")
        if mtch.Success then
            let g = mtch.Groups.[2]
            scan (text.Remove (g.Index, g.Length))
        else
            text

    let main () =
        let text = Console.ReadLine ()
        Console.WriteLine (scan text)
        ()

module M7 =
    open System.Drawing

    let colors = [
        Brushes.Red
        Brushes.Orange
        Brushes.Yellow
        Brushes.Green
        Brushes.Blue
        Brushes.Purple
    ]

    let rec draw (img : Graphics) x0 y0 size (colors : Brush list) =
        if size = 1 then
            ()
        else
            let s = size/3
            let x1 = x0 + s
            let x2 = x1 + s
            let y1 = y0 + s
            let y2 = y1 + s
            img.FillRectangle (colors.Head, x1, y1, s, s)
            draw img x0 y0 s colors.Tail
            draw img x0 y1 s colors.Tail
            draw img x0 y2 s colors.Tail

            draw img x1 y0 s colors.Tail
            draw img x1 y2 s colors.Tail

            draw img x2 y0 s colors.Tail
            draw img x2 y1 s colors.Tail
            draw img x2 y2 s colors.Tail

    let main () =
        let levels = 6
        let size = pown 3 levels
        use img = new Bitmap (size, size)
        use gr = Graphics.FromImage img
        gr.FillRectangle (Brushes.Black, 0, 0, size, size)
        draw gr 0 0 size colors
        img.Save "out.png"
        ()

module M8 =
    let wordv = [
        0, "zero"
        1, "one"
        2, "two"
        3, "three"
        4, "four"
        5, "five"
        6, "six"
        7, "seven"
        8, "eight"
        9, "nine"
        10, "ten"
        11, "eleven"
        12, "twelve"
        13, "thirteen"
        14, "fourteen"
        15, "fifteen"
        16, "sixteen"
        17, "seventeen"
        18, "eighteen"
        19, "nineteen"
        20, "twenty"
        30, "thirty"
        40, "forty"
        50, "fifty"
        60, "sixty"
        70, "seventy"
        80, "eighty"
        90, "ninety"
    ]
    let words = dict wordv

    // divmod
    let (/%) a b =
        a/b, a%b

    let rec intToEnglish n =
        if n < 20 then
            words.[n]
        elif n < 100 then
            if n % 10 = 0 then
                words.[n]
            else
                let q, r = n /% 10
                words.[10*q] + "-" + words.[r]
        elif n < 1000 then
            if n % 100 = 0 then
                words.[n/100] + " hundred"
            else
                let q, r = n /% 100
                words.[q] + " hundred and " + intToEnglish r
        elif n < 1000000 then
            if n % 1000 = 0 then
                words.[n/1000] + " thousand"
            else
                let q, r = n /% 1000
                intToEnglish q + " thousand, " + intToEnglish r
        else
            "one million"

    let main () =
        Console.Write "Input: "
        let n = int (Console.ReadLine ())
        Console.WriteLine (intToEnglish n)
        ()

module M9 =
    open System.IO

    let strreplace (old : string) (neww : string) (str : string) =
        str.Replace (old, neww)

    let filewrite filename contents =
        File.WriteAllText (filename, contents)

    let main () =
        Console.WriteLine "File to scan: "
        let filename = Console.ReadLine ()
        Console.WriteLine "String to replace: "
        let find = Console.ReadLine ()
        Console.WriteLine "Replacement: "
        let replace = Console.ReadLine ()
        File.ReadAllText filename |> strreplace find replace |> filewrite filename
        ()

        
module M10 = 
    // skipping because it's not interesting
    ()
