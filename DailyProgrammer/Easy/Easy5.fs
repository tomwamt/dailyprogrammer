﻿namespace DailyProgrammer.Easy

open System

module E21 =
    let rec swap n li =
        match li with
        | [] -> failwith "Invalid"
        | [head] -> [head; n]
        | first :: second :: rest ->
            if second <= n then
                first :: n :: second :: rest
            else
                let f :: s :: t = swap n (second :: rest)
                f :: first :: s :: t

    let rec nextup = function
        | [] -> failwith "Invalid2"
        | [head] -> [head], false
        | head :: tail ->
            match nextup tail with
            | [], _ -> failwith "Invalid3"
            | l, true -> head :: l, true
            | first :: rest, false ->
                if head >= first then
                    head :: first :: rest, false
                else
                    let h :: t = swap head (first :: rest)
                    h :: (List.sort t), true

    let main (args : string []) =
        args.[0].ToCharArray ()
        |> List.ofArray
        |> nextup
        |> fst
        |> Array.ofList
        |> String
        |> Console.WriteLine
        ()

module E22 =
    let listMerge lst1 lst2 =
        List.append
            lst1
            (lst2 |> List.filter (fun x -> not (List.contains x lst1)))

    let main args =
        let l1 : obj list = ["a"; "b"; "c"; 1; 4]
        let l2 : obj list = ["a"; "x"; 34; "4"]
        printfn "%A" (listMerge l1 l2)
        ()

module E23 =
    let main args =
        let lst = Console.ReadLine().Split ' '
        let m = lst.Length / 2
        printfn "First half: %A" lst.[..m-1]
        printfn "Second half: %A" lst.[m..]
        ()

module E25 =
    let main (args : string []) =
        let votes = Array.map int args
        let total = Array.sum votes
        let maj = votes |> Array.tryFindIndex (fun v -> v*2 > total)
        match maj with
        | Some(i) -> Console.WriteLine ("Majority: " + string i)
        | None -> Console.WriteLine "No majority"
        ()
