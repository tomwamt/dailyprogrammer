﻿namespace DailyProgrammer.Easy

open System

module E16 =
    let main (args : string []) =
        let blacklist = args.[1].ToCharArray ()
        args.[0].ToCharArray ()
        |> Array.filter (fun c -> not (Array.contains c blacklist))
        |> String
        |> Console.WriteLine
        ()

module E17 =
    // Question was deleted, so this is revese-engineered
    // from the submitted solutions & discussion
    let main (args : string []) =
        let n = int args.[0]
        seq { 0..(n-1) }
        |> Seq.iter (fun i -> Console.WriteLine (String.replicate (pown 2 i) "@"))
        ()

module E18 =
    let translate = function
    | 'A' | 'B' | 'C'       -> '2'
    | 'D' | 'E' | 'F'       -> '3'
    | 'G' | 'H' | 'I'       -> '4'
    | 'J' | 'K' | 'L'       -> '5'
    | 'M' | 'N' | 'O'       -> '6'
    | 'P' | 'Q' | 'R' | 'S' -> '7'
    | 'T' | 'U' | 'V'       -> '8'
    | 'W' | 'X' | 'Y' | 'Z' -> '9'
    | d when Char.IsDigit d -> d
    | _ -> ' '

    let main args =
        let input = Console.ReadLine ()
        let tmp =
            input.ToCharArray ()
            |> Array.filter (fun c -> c <> '-')
            |> Array.map (fun c -> c |> Char.ToUpper |> translate)
            |> String
        Console.WriteLine (tmp.[0..0] + "-" + tmp.[1..3] + "-" + tmp.[4..6] + "-" + tmp.[7..10])
        ()

module E19 =
    open System.IO
    open System.Text.RegularExpressions

    let main args =
        let book = File.ReadAllLines "Easy/sherlock.txt"
                   |> Array.filter (fun line -> line <> line.ToUpper ()) // all caps lines are titles
                   |> String.Concat
        let count = Regex.Replace(book, "\W", "").Length

        Console.WriteLine count
        ()

module E20 =
    // Call M14 with arg.[0] = 999
    let main args = ()
