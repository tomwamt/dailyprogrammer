﻿namespace DailyProgrammer.Easy

open System

module E1 = 
    let main() =
        printf "Enter your name: "
        let name = Console.ReadLine()
        printf "Enter your age: "
        let age = Console.ReadLine()
        printf "Enter your username: "
        let username = Console.ReadLine()
        printfn "Your name is %s, you are %s years old, and your username is %s" name age username
        0

module E2 = 
    let main() = 
        printf "Enter force: "
        let f = Console.ReadLine() |> double
        printf "Enter mass: "
        let m = Console.ReadLine() |> double
        printfn "Your acceleration is %f" (f/m)

module E3 =
    let e c rot = 
        if c = ' ' then
            ' '
        else
            char ((int c - 97 + rot) % 26 + 97)

    let encrypt text rot =
        String([| for c in text -> e c rot |])

    let d c rot =
        if c = ' ' then
            ' '
        else
            char ((int c - 97 + (26 - rot)) % 26 + 97)
    
    let decrypt text rot =
        String([| for c in text -> d c rot |])

    let main () =
        printf "Enter a message: " 
        let msg = (Console.ReadLine ()).ToLower ()
        printf "Enter a rot number: "
        let rot = int (Console.ReadLine ())
        printfn "Message: %s" msg
        let emsg = encrypt msg rot
        printfn "Encrypted: %s" emsg
        printfn "Decrypted: %s" (decrypt emsg rot)
        ()

module E4 =
    let alpha = "abcdefghijklmnopqrstuvqxyz0123456789!@#$%^&*".ToCharArray ()

    let main () =
        let n = alpha.Length
        Console.Write "Enter password length: "
        let l =  int (Console.ReadLine ())
        let r = Random ()
        let passwd = String.Concat (seq { for i in 1..l -> alpha.[r.Next alpha.Length] })
        Console.WriteLine passwd
        ()

module E5 =
    let readPass () =
        let mutable pass = ""
        let mutable key = Console.ReadKey ()
        while key.Key <> ConsoleKey.Enter do
            if key.Key = ConsoleKey.Backspace then
                pass <- pass.[0..pass.Length-1]
            else
                pass <- pass + string key.KeyChar
                Console.Write "\b"
                key <- Console.ReadKey ()
        pass

    let main () =
        let f = IO.File.ReadAllLines "Easy/userpass.txt"
        let user = f.[0]
        let pass = f.[1]
        let mutable running = true
        while running do
            Console.Write "Username: "
            let userin = Console.ReadLine ()
            if userin = user then
                Console.Write "Password: "
                let passin = readPass ()
                if passin = pass then
                    Console.WriteLine "User authenticated"
                    running <- false
                else
                    Console.WriteLine "Invalid password!"
            else
                Console.WriteLine "Unknown user."
        ()