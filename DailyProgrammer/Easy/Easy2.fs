﻿namespace DailyProgrammer.Easy

open System

// Warning: This program uses 2+Gb memory and takes several minutes to finish
module E6 =
    let main () =
        let big3 = bigint.Parse "300000000000000000000000000000000000000000000000000"
        let big4 = bigint.Parse "400000000000000000000000000000000000000000000000000"
        let small2 = bigint 2
        let small3 = bigint 3
        let small4 = bigint 4
        let n = int (Console.ReadLine ())

        let piwf (i : int) =
            async {
                let bigi = bigint i
                let d = (small2*bigi + small2) * (small2*bigi + small3) * (small2*bigi + small4)
                if i%2 = 0 then
                    return big4 / d
                else
                    return -(big4 / d)
            }

        let pi = big3 + (seq { for i in 0 .. n -> piwf i } |> Async.Parallel |> Async.RunSynchronously |> Array.reduce (+))

        let pistr = string pi
        Console.Write pistr.[0]
        Console.Write "."
        Console.WriteLine pistr.[1..]
        ()

module E7 =
    let table = [
        ' ', "/"
        'A', ".-"
        'B', "-..."
        'C', "-.-."
        'D', "-.."
        'E', "."
        'F', "..-."
        'G', "--."
        'H', "...."
        'I', ".."
        'J', ".---"
        'K', "-.-"
        'L', ".-.."
        'M', "--"
        'N', "-."
        'O', "---"
        'P', ".--."
        'Q', "--.-"
        'R', ".-."
        'S', "..."
        'T', "-"
        'U', "..-"
        'V', "...-"
        'W', ".--"
        'X', "-..-"
        'Y', "-.--"
        'Z', "--.."
        '0', "-----"
        '1', ".----"
        '2', "..---"
        '3', "...--"
        '4', "....-"
        '5', "....."
        '6', "-...."
        '7', "--..."
        '8', "---.."
        '9', "----."
        '.', ".-.-.-"
    ]

    let morse = dict table
    let morseinv = dict ((Seq.map (fun (a, b) -> b, a)) table)

    let main () =
        Console.Write "(1) Morse to string or (2) String to morse: " 
        let mode = int (Console.ReadLine ())
        match mode with
        | 1 ->
            Console.Write "Input: "
            let letters = (Console.ReadLine ()).Split ' '
            let translated = String([| for m in letters -> morseinv.[m] |])
            Console.WriteLine translated
        | 2 ->
            Console.Write "Input: "
            let letters = ((Console.ReadLine ()).ToUpper ()).ToCharArray ()
            let translated = letters |> Array.map (fun c -> morse.[c]) |> String.concat " "
            Console.WriteLine translated
        | _ -> ()
        ()

module E8 =
    let rec bottlesOfBeerOnTheWall n =
        if n > 1 then
            Console.Write (string n + " bottles of beer on the wall, " + string n + " bottles of beer... ")
            Console.WriteLine ("Take one down, pass it around, " + string (n-1) + " bottles of beer on the wall!")
            n-1 |> bottlesOfBeerOnTheWall
        else
            Console.Write "1 bottle of beer on the wall, 1 bottle of beer... "
            Console.WriteLine "Take one down, pass it around, no more bottles of beer on the wall!"

    let main () =
        // Using pipe syntax because it sounds like the song!
        99 |> bottlesOfBeerOnTheWall
        ()

module E9 =
    let strsplit sep (str : string) =
        str.Split sep

    let main () =
        Console.ReadLine () |> strsplit [| ' ' |] |> Array.sort |> Array.iter Console.WriteLine
        ()

module E10 =
    open System.Text.RegularExpressions

    let isTelephone str =
        Regex.IsMatch (str, @"^(\(\d{3}\)|\d{3})?[ \-.]?\d{3}[ \-.]?\d{4}$")

    let main () =
        Console.Write "Enter input: "
        let input = Console.ReadLine ()
        Console.WriteLine ("That " + (if isTelephone input then "is" else "is not") + " a telephone number.")
        ()
