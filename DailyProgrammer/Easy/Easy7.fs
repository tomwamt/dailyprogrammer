﻿namespace DailyProgrammer.Easy

open System

module E31 =
    let table = 
        dict (Array.mapi (fun i c -> c, i) ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()))

    let ctoi c =
        table.[c]

    let itoc i =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ".[i]

    let parse (s : string) =
        Array.fold (fun acc c -> acc*26 + ctoi c) 0 (s.ToCharArray())

    let encode =
        (Array.unfold (fun n ->
            if n = 0 then
                None
            else
                let c = itoc (n % 26)
                Some(c, n/26)
        )) >> Array.rev >> String

    let main (args : string []) =
        let a = parse args.[0]
        let b = parse args.[1]
        Console.WriteLine (encode (a*b))
        ()
