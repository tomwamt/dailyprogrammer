﻿namespace DailyProgrammer.Easy

open System

module E26 =
    let rec chunkBySame (li : 'a list) =
        if li.IsEmpty then
            []
        else
            let x = li.Head
            let chunk = List.takeWhile ((=) x) li
            chunk :: (li |> List.skipWhile ((=) x) |> chunkBySame)

    let main (args : string []) =
        Console.WriteLine args.[0]
        args.[0].ToCharArray()
        |> List.ofArray
        |> chunkBySame
        |> List.fold (fun (s1, s2) ch -> (s1 + Char.ToString ch.Head, s2 + (ch.Tail |> Array.ofList |> String))) ("", "")
        |> (fun (s1, s2) ->
            Console.WriteLine s1
            Console.WriteLine s2
        )
        ()

module E27 =
    let main (args : string []) =
        Console.Write "Enter year: "
        let year = Console.ReadLine () |> int
        Console.WriteLine ("Century: " + string ((year - 1) / 100 + 1))
        Console.WriteLine ("Leap year: " + string (DateTime.IsLeapYear year))
        ()

module E28 =
    open System.Collections.Generic

    let arrSwap a b array = 
        let t = Array.get array a
        array.[a] <- array.[b]
        array.[b] <- t

    let arrShuffleInPlace (random : Random) array =
        let n = Array.length array - 1
        for i = n downto 0 do 
            arrSwap i (random.Next i) array
        array

    let arrFindDuplicate array =
        let s = HashSet<'a>()
        Array.find (s.Add >> not) array

    let main (args : string []) =
        let n = int args.[0]
        let r = Random()
        let arr = Array.init n (fun i -> if i = n-1 then r.Next n else i) |> arrShuffleInPlace r
        Console.WriteLine (arrFindDuplicate arr)
        ()

module E29 =
    let main (args : string []) =
        Console.Write "Enter string: "
        let chars = Console.ReadLine().ToLower().ToCharArray() |> Array.filter (fun c -> c <> ' ')
        if chars = Array.rev chars then
            Console.WriteLine "That is a palindrome"
        else
            Console.WriteLine "That is not a palindrome"
        ()

module E30 =
    open System.Collections.Generic

    let canSum (ns : int list) (target : int) =
        let s = HashSet<int>()
        let ao =
            List.tryFind (fun x ->
                let c = s.Contains x
                s.Add (target - x) |> ignore
                c
            ) ns
        match ao with
        | Some(a) -> Some(a, target-a)
        | None -> None

    let main (args : string[]) =
        let l = [4; 23; 65; 2; 8; 9; 14; 6; 1]
        let x = canSum l 16
        match x with
        | Some(a,b) -> Console.WriteLine(string a + ", " + string b)
        | None -> Console.WriteLine("Not found")
        ()
