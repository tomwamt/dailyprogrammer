﻿namespace DailyProgrammer.Easy

open System

module E11 =
    let main () =
        Console.Write "Day: "
        let day = int (Console.ReadLine ())
        Console.Write "Month: "
        let month = int (Console.ReadLine ())
        Console.Write "Year: "
        let year = int (Console.ReadLine ())
        let dt = DateTime (year, month, day)
        Console.WriteLine dt.DayOfWeek
        ()

// Decided to hand write a permutation generator
module E12 =
    let arrWith (first : 'a) (rest : 'a []) =
        Array.append [| first |] rest

    let arrWithout (a : 'a []) i =
        if a.Length = 0 then
            Array.empty
        elif i = 0 then
            a.[1..]
        elif i = a.Length - 1 then
            a.[..i-1]
        else
            Array.append a.[..i-1] a.[i+1..]

    let rec permutations (arr : 'a []) =
        if arr.Length = 1 then
            Set.singleton arr
        else
            (seq { 0..arr.Length-1 })
            |> Seq.map (fun i -> Set.map (arrWith arr.[i]) (permutations (arrWithout arr i)))
            |> Set.unionMany

    let main (args : string []) = 
        permutations (args.[0].ToCharArray ()) |> Set.map String |> Set.iter Console.WriteLine
        ()

module E13 =
    let main args =
        Console.Write "Enter month: "
        let month = int (Console.ReadLine ())
        Console.Write "Enter day: "
        let day = int (Console.ReadLine ())
        let dt = DateTime (DateTime.Now.Year, month, day)
        Console.WriteLine dt.DayOfYear
        ()

module E14 =
    let main args =
        Console.WriteLine "Enter list:"
        let input = (Console.ReadLine ()).Split (' ')
        Console.Write "Chunk size: "
        let chunk = int (Console.ReadLine ())
        input
        |> Array.chunkBySize chunk
        |> Array.map Array.rev
        |> Array.concat
        |> Array.iter (fun x -> Console.Write (x + " "))
        Console.WriteLine ()
        ()

module E15 =
    open System.IO

    let main (args : string []) =
        let lines = File.ReadAllLines args.[0]
        match args.[1] with
        | "left" ->
            lines |> Array.iter (fun s -> Console.WriteLine (s.Trim ()))
        | "right" ->
            let maxLen = lines |> Array.maxBy String.length |> String.length
            lines |> Array.iter (fun s -> Console.WriteLine (s.Trim().PadLeft maxLen))
        | _ -> ()
        ()
